﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace icshp_cv07
{
    /// <summary>
    ///  Třída Form1.
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        ///  Konstruktor třídy Form1
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  Pomocná metoda pro Zachování poměru stran daného obrázku.
        /// </summary>
        /// <param name="bmp">Původní obrázek</param>
        /// <returns>Obrázek ve správném poměru stran ke komponentě PictureBox</returns>
        private Bitmap ResizeImage(Bitmap bmp) {
            var ratioX = (double)pictureBox_picture.Width / bmp.Width;
            var ratioY = (double)pictureBox_picture.Height / bmp.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(bmp.Width * ratio);
            var newHeight = (int)(bmp.Height * ratio);

            sizeToolStripMenuItem.Text = "Šířka: " + newWidth + "px Výška: " + newHeight + "px";

            return new Bitmap(bmp, newWidth, newHeight);
        }

        /// <summary>
        /// Handler události - Kliknutí na tlačítko "Načti obrázek"
        /// Načte obrázek a vloží ho do komponenty PictureBox
        /// </summary>
        private void loadPictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sizeToolStripMenuItem.Text = "";
            pictureBox_picture.Image = null;
            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Pcx pcx = new Pcx();
                    pictureBox_picture.Image = ResizeImage(pcx.LoadPCX(openFileDialog.FileName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}

 