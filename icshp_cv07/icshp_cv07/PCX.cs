﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace icshp_cv07
{
    /// <summary>
    /// Třída PCX, která umožnuje dekódovat .pcx pomocí RLE a převést na bitmap.
    /// </summary>
    class Pcx
    {
        private byte Identifier;        /* PCX Id Number (Always 0x0A) */
        private byte Version;           /* Version Number */
        private byte Encoding;          /* Encoding Format */
        private byte BitsPerPixel;      /* Bits per PiteraceXel */
        private UInt16 XStart;          /* Left of image */
        private UInt16 YStart;          /* Top of Image */
        private UInt16 XEnd;            /* Right of Image*/
        private UInt16 YEnd;            /* Bottom of image */
        private UInt16 HorzRes;         /* Horizontal Resolution */
        private UInt16 VertRes;         /* Vertical Resolution */
        private byte[] Palette;         /* 16-byt2 EGA Palette [48] */
        private byte Reserved1;         /* Reserved (Always 0) */
        private byte NumBitPlanes;      /* Number of Bit Planes */
        private UInt16 BytesPerLine;    /* Bytes per Scan-line */
        private UInt16 PaletteType;     /* Palette Type */
        private UInt16 HorzScreenSize;  /* Horizontal Screen Size */
        private UInt16 VertScreenSize;  /* Vertical Screen Size */
        private byte[] Reserved;        /* Reserved (Always 0) [54]*/

        private long MaxNumberOfbyts;
        private int ScanLineLength;

        private int ImageWidth;         /* Width of image in pixels */
        private int ImageHeight;        /* Length of image in scan lines */
        private int LinePaddingSize;

        private readonly List<byte> PictureData;
        private readonly List<byte> PalleteData;

        /// <summary>
        /// Konstruktor třídy Pcx();
        /// </summary>
        public Pcx()
        {
            Palette = new byte[48];
            Reserved = new byte[54];
            PictureData = new List<byte>();
            PalleteData = new List<byte>();
        }

        /// <summary>
        /// Veřejná metoda, která nejdříve dekóduje .pcx a pak jej načte do paměti.
        /// V dalším kroku načtený obrázek převede na Bitmap a vrátí ho.
        /// </summary>
        /// <param name="filename">Cesta k souboru</param>
        /// <returns>Bitmap</returns>
        public Bitmap LoadPCX(string filename)
        {
            try
            {
                ReadFile(filename); //Načtení obrázku do paměti
                return GetBitmap(); //Přeformátování do bitmapy
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Pomocná metoda pro volání ostatních metod, které postupně načítají obrázky do paměti
        /// </summary>
        /// <param name="filename">Cesta k souboru</param>
        private void ReadFile(string filename)
        {
            try
            {
                using (BinaryReader binaryReader = new BinaryReader(File.Open(filename, FileMode.Open)))
                {
                    ReadHeader(binaryReader);
                    // ShowHeaderInformation(); - Slouží pouze pro vývoj, diagnostika typu obrázku.
                    RLEDecoder(binaryReader);
                    //WriteDecompresedPCX(filename); - Slouží pouze pro otestování dekomprese!
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metoda sloužící k načtení hlavičky souboru
        /// </summary>
        /// <param name="binaryReader">BinaryReader</param>
        private void ReadHeader(BinaryReader binaryReader)
        {
            //Header
            Identifier = binaryReader.ReadByte();
            Version = binaryReader.ReadByte();
            Encoding = binaryReader.ReadByte();
            BitsPerPixel = binaryReader.ReadByte();
            XStart = binaryReader.ReadUInt16();
            YStart = binaryReader.ReadUInt16();
            XEnd = binaryReader.ReadUInt16();
            YEnd = binaryReader.ReadUInt16();
            HorzRes = binaryReader.ReadUInt16();
            VertRes = binaryReader.ReadUInt16();
            Palette = binaryReader.ReadBytes(48);
            Reserved1 = binaryReader.ReadByte();
            NumBitPlanes = binaryReader.ReadByte();
            BytesPerLine = binaryReader.ReadUInt16();
            PaletteType = binaryReader.ReadUInt16();
            HorzScreenSize = binaryReader.ReadUInt16();
            VertScreenSize = binaryReader.ReadUInt16();
            Reserved = binaryReader.ReadBytes(54);
            //Informace navíc
            MaxNumberOfbyts = (1L << (BitsPerPixel * NumBitPlanes));
            ScanLineLength = (BytesPerLine * NumBitPlanes);
            ImageWidth = XEnd - XStart;
            ImageHeight = YEnd - YStart;
            LinePaddingSize = ((BytesPerLine * NumBitPlanes) * (8 / BitsPerPixel)) - ((XEnd - XStart) + 1);
        }

        /// <summary>
        /// Metoda sloužící pro dekódování (RLE encoding).
        /// </summary>
        /// <param name="reader">BinaryReader</param>
        private void RLEDecoder(BinaryReader reader)
        {
            List<byte> data = new List<byte>();
            int iteraceX;
            int iteraceY = 0;
            do
            {
                iteraceY++;
                for (iteraceX = 0; iteraceX < ScanLineLength;)
                {
                    byte byt = reader.ReadByte();
                    if (byt > 192)
                    {
                        byt -= 192;
                        byte byt2 = reader.ReadByte();

                        if (iteraceX <= ScanLineLength)
                        {
                            for (byte x = 0; x < byt; x++)
                            {
                                PictureData.Add(byt2);
                                iteraceX++;
                            }
                        }
                        else
                            iteraceX += byt;
                    }
                    else
                    {
                        if (iteraceX <= ScanLineLength)
                            PictureData.Add(byt);
                        iteraceX++;
                    }
                }
            } while (iteraceY < ImageHeight);

            //Pokud existuje šablona za obrazovými daty.
            while (!(reader.BaseStream.Position == reader.BaseStream.Length))
            {
                PalleteData.Add(reader.ReadByte());
            }
        }

        /// <summary>
        /// Metoda sloužící pro převod z formátu .pcx na Bitmap
        /// </summary>
        /// <returns>Bitmap</returns>
        private Bitmap GetBitmap()
        {
            Bitmap bitmap = new Bitmap((int)ImageWidth, (int)ImageHeight, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            int x, y, i, position = 0;

            if (BytesPerLine == 0) BytesPerLine = Convert.ToUInt16(ImageWidth);

            //U obrázků s dodatečnou paletou, načti paletu a ignoruj prázdné.
            if (BitsPerPixel == 8 && NumBitPlanes == 1)
            {
                Palette = new byte[768];
                int index = PalleteData.Count - 768;
                for (int v = PalleteData.Count - 768; v < PalleteData.Count; v++)
                {
                    Palette[v - index] = PalleteData[v];
                }
            }

            try
            {
                if (NumBitPlanes == 1 && BitsPerPixel == 8)
                {
                    byte[] scanline = new byte[BytesPerLine];
                    for (y = 0; y < ImageHeight; y++)
                    {
                        for (i = 0; i < BytesPerLine; i++)
                            scanline[i] = PictureData[position++];

                        for (x = 0; x < ImageWidth; x++)
                        {
                            i = scanline[x];
                            bitmap.SetPixel(x, y, Color.FromArgb(Palette[i * 3], Palette[i * 3 + 1], Palette[i * 3 + 2]));
                        }
                    }
                }
                else if (NumBitPlanes == 3)
                {
                    byte[] scanLineRed = new byte[BytesPerLine];
                    byte[] scanLineGreen = new byte[BytesPerLine];
                    byte[] scanLineBlue = new byte[BytesPerLine];

                    for (y = 0; y < ImageHeight; y++)
                    {
                        for (i = 0; i < BytesPerLine; i++)
                            scanLineRed[i] = PictureData[position++];
                        for (i = 0; i < BytesPerLine; i++)
                            scanLineGreen[i] = PictureData[position++];
                        for (i = 0; i < BytesPerLine; i++)
                            scanLineBlue[i] = PictureData[position++];

                        for (int n = 0; n < ImageWidth; n++)
                        {
                            bitmap.SetPixel(n, y, Color.FromArgb(scanLineRed[n], scanLineGreen[n], scanLineBlue[n]));
                        }
                    }
                }
                else
                {
                    throw new Exception("Tento typ obrázku není podporován!");
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return bitmap;
        }

        #region Pomocné metody pro testování při vývoji
        /// <summary>
        /// Pomocná metoda pro prvotní testování funkčnosti dekódování.
        /// Vytvoří .pcx soubor, který není zmenšený pomocí RLE
        /// </summary>
        /// <param name="fileName">Jméno souboru/cesta pro zápis</param>
        private void WriteDecompresedPCX(string fileName)
        {
            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(fileName + "_.pcx", FileMode.Create)))
                {
                    writer.Write(Identifier);
                    writer.Write(Version);
                    writer.Write((byte)0);
                    writer.Write(BitsPerPixel);
                    writer.Write(XStart);
                    writer.Write(YStart);
                    writer.Write(XEnd);
                    writer.Write(YEnd);
                    writer.Write(HorzRes);
                    writer.Write(VertRes);
                    writer.Write(Palette);
                    writer.Write(Reserved1);
                    writer.Write(NumBitPlanes);
                    writer.Write(BytesPerLine);
                    writer.Write(PaletteType);
                    writer.Write(HorzScreenSize);
                    writer.Write(VertScreenSize);
                    writer.Write(Reserved);
                    foreach (byte item in PictureData)
                    {
                        writer.Write(item);
                    }
                    foreach (byte item in PalleteData)
                    {
                        writer.Write(item);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Pomocná metoda pro prvotní testování funkčnosti načítání hlavičky.
        /// Vypisuje data z hlavičky do Debug console.
        /// </summary>
        private void ShowHeaderInformation()
        {
            Debug.Write("PaintBrush software release: ");
            switch (Version)
            {
                case 0:
                    Debug.WriteLine("PC Paintbrush version 2.5 using a fixed EGA palette");
                    break;
                case 2:
                    Debug.WriteLine("PC Paintbrush version 2.8 using a modifiable EGA palette");
                    break;
                case 3:
                    Debug.WriteLine("PC Paintbrush version 2.8 using no palette");
                    break;
                case 4:
                    Debug.WriteLine("PC Paintbrush for Windows");
                    break;
                case 5:
                    Debug.WriteLine("PC Paintbrush version 3.0, including 24-bit images");
                    break;
                default:
                    Debug.WriteLine("Není uvedeno!");
                    break;
            }
            Debug.Write("Encoding: ");
            switch (Encoding)
            {
                case 0:
                    Debug.WriteLine("No encoding (rarely used)");
                    break;
                case 1:
                    Debug.WriteLine("Run-length encoding (RLE)");
                    break;
                default:
                    Debug.WriteLine("Není uvedeno!");
                    break;
            }
            Debug.Write("Počet bitů na pixel v obrazové rovině: ");
            switch (BitsPerPixel)
            {
                case 1:
                    Debug.WriteLine("The image has two byt2s (monochrome)");
                    break;
                case 2:
                    Debug.WriteLine("The image has four byts");
                    break;
                case 4:
                    Debug.WriteLine("The image has 16 byts");
                    break;
                case 5:
                    Debug.WriteLine("The image has 256 byts");
                    break;
                default:
                    Debug.WriteLine("Není uvedeno!");
                    break;
            }
            Debug.WriteLine("Start X:" + XStart);
            Debug.WriteLine("Start Y:" + YStart);
            Debug.WriteLine("Stop X:" + XEnd);
            Debug.WriteLine("Stop Y:" + YEnd);
            Debug.WriteLine("Horizontal DPI:" + HorzRes);
            Debug.WriteLine("Vertical DPI:" + VertRes);
            Debug.WriteLine("The first reserved field:" + Palette);
            Debug.WriteLine("The number of byt2 planes constituting the piteraceXel data:" + NumBitPlanes);
            Debug.WriteLine("The number of bytes of one byt2 plane representing a single scan line:" + BytesPerLine);

            switch (PaletteType)
            {
                case 1:
                    Debug.WriteLine("The palette contains monochrome or byt information");
                    break;
                case 2:
                    Debug.WriteLine("The palette contains grayscale information");
                    break;
                default:
                    Debug.WriteLine("Není uvedeno co palette obsahuje!");
                    break;
            }

            Debug.WriteLine("Moje data:");
            Debug.WriteLine("MaxNumber Of byt2s: " + MaxNumberOfbyts);
            Debug.WriteLine("ScanLine length: " + ScanLineLength);
            Debug.WriteLine("Image width: " + ImageWidth);
            Debug.WriteLine("Image height: " + ImageHeight);
            Debug.WriteLine("ScanLine padding:" + LinePaddingSize);
        }
#endregion
    }
}
